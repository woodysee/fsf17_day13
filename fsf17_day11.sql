SELECT * FROM day11db.books;

# Exact match (Criterion)
SELECT * FROM day11db.books WHERE year_revised = 2014;
SELECT * FROM day11db.books WHERE isbn13 = '9781442499560';
SELECT * FROM day11db.books WHERE isbn13 = '9781442499560' AND year_published = 2014;

# Exact match (Criteria)
SELECT * FROM day11db.books WHERE isbn13 = '9781442499560' OR isbn13 = '9781442499561';
SELECT * FROM day11db.books
	WHERE 	(name = 'Random 2' AND isbn13 = '9781442499561')
    OR 		(name = 'Random 1' AND isbn13 = '9781442499560');

select isbn13, revision, edition from day11db.books
where cover_front is null and cover_back is null; #narrow return

select 1+2+3 as totalSum; #calculator functions

select NOW() as today, 1+2 as totalSum, isbn13 from day11db.books; #today's date

select * from day11db.books where price_digital > 5;

select * from day11db.books limit 5 offset 2; #limits 5 records and skips the first n records in offset

#in has to be exact, while like can take substring

select name from day11db.books where isbn13 in ('9781442499561','9781442499560'); #don't use too many as it will crash the server

select * from day11db.books where isbn13 like '%563%'; # percent symbol used to denote start and end of a substring, i.e. xxx563xxx

select * from day11db.books where isbn13 like '%561'; # without percent symbol at end, it will only match for strings at the end of the field

select * from day11db.books where isbn13 like '%ando%4'; # search for substring "ando", and substring "4" only at the end

select * from day11db.books where upper(name) like upper('%lol'); #upper searches for uppercase

select * from day11db.books order by id; #default ascending order of id

select * from day11db.books order by id desc; #descending! see below.

desc day11db.books; #description! see above.

select * from day11db.books order by name asc, name;

select name as 'Random 5' from day11db.books order by name desc, id;

select concat(name, '---', id) as new_column from day11db.books order by name asc, id; #MySQL specific syntax

select distinct price_hardcopy from day11db.books; #see only unique price_hardcopy
select price_hardcopy, count(*) from day11db.books group by price_hardcopy; #aggregates the number of unique price_hardcopy in db

select sum(price_hardcopy) from day11db.books; #sum up the prices for each copy of the book

select min(price_hardcopy) from day11db.books;
select max(price_hardcopy) from day11db.books;
select avg(price_hardcopy) from day11db.books;
select std(price_hardcopy) from day11db.books;

#functions store custom sql methods defined by the user

select price_hardcopy, count(*) as noOfCopies from day11db.books group by price_hardcopy having noOfCopies > 0;

select a1.name, a1.author as firstAuthor, a2.name, a2.author
as secondAuthor
from
	(select * from day11db.books where author = 'Tom Leveen') a1,
    (select * from day11db.books where author = 'Tom Leveen') a2;