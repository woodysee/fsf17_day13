# Simple Sequelize

Check the commit history.

CRUD for `sequelize` used with a MySQL database.

1. Download and unzip the dataset `test_db-master/`.
2. From the unzipped `test_db-master/`, look for `/employees.sql`.
3. Take note of the path to `/employees.sql` and to `/sakila-mv-schema.sql`.
4. Go to the directory where `/employees.sql` is.
5. Login as root (could login as the user you created too, if it has admin permission). Change to the inflated test master directory before running the command below.
```
mysql -uroot -p
```

6. Create a new database "Employees", which is empty. Type the following command in the terminal.
```
CREATE DATABASE employees;
```

7. Check that your database is created:
```
SHOW DATABASES;
```

8. Select employees database.
```
USE employees;
```

9. Type the following command import data to the database "Employees" you create (as root or any user with create permission).
```
SOURCE employees.sql
```

10. Display the list of tables created.
```
SHOW TABLES;
```

11. Create a sakila database. You create this the same way you created the Employees database.

12. Ensure that you're using the sakila database. (HINT: USE `<database_name>` )

# References
- Add MySQL to terminal shell in Mac OS X: http://www.gigoblog.com/2011/03/13/add-mysql-to-terminal-shell-in-mac-os-x/
- Dataset: https://github.com/datacharmer/test_db
- Sequelize: http://docs.sequelizejs.com/manual/installation/getting-started.html
