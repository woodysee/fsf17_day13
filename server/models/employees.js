module.exports = function(connection,Sequelize) {
    // Performing a mapping of Employees object
    // Return object is used within the script
    const Employees = connection.define('employees', {
        //Now we copy all the columns over from mySQL:
        emp_no: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        birth_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        gender: {
            type: Sequelize.ENUM('M', 'F'),
            allowNull: false
        },
        hire_date: {
            type: Sequelize.DATE,
            allowNull: false
        }
    }, {
        timestamps: false
    });
    return Employees;
};