const Sequelize = require("sequelize");
const SQL_USERNAME = "root";
const SQL_PASSWORD = "";
//Initialise the connection
const connection = new Sequelize("employees", SQL_USERNAME, SQL_PASSWORD, {
  host: "localhost",
  port: 3306,
  logging: false,
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

const Op = Sequelize.Op; //LIKE, WHERE sql operators. Full reference at http://docs.sequelizejs.com/manual/tutorial/querying.html#operators

//Important: Arguments must also be in the same import load order
const Employees = require("./models/employees")(connection, Sequelize);
console.log(Employees);

Employees.findAll({limit: 10}).then(
  (results)=>{
    console.log("------PRINTING ALL...--------");
    console.log(results);
    results.forEach(
      (el) => {
        console.log(el.emp_no);
        // Getting records without Sequelize select statements
        console.log(el.birth_date);
      }, this
    );
  }
).catch(
  (error) => {
    console.log(error);
  }
);

Employees.findOne({where: {emp_no: 10008}}).then(
  (results) => {
    console.log("------FINDING ONE ONLY...--------");
    console.log(results);
  }
).catch(
  (error) => {
    console.log(error);
  }
);

Employees.findOne(
  {
    attributes:
    [
      'emp_no',
      'gender',
      [
        'gender',
        'GENDERDEF'
      ]
    ],
    where: {
      emp_no: 10008
    }
  }
).then(
  (result) => {
    console.log(result);
    console.log("------Find one (PROJECTION)...--------");
    console.log(result.emp_no);
    console.log(result.gender);
    console.log(result.GENDER2);
  }
).catch(
  (error) => {
    console.log(error);
  }
);

Employees.findOne(
  {
    attributes: {
      exclude: 'gender'
    }, where: {
      emp_no: 10008
    }
  }
).then(
  (result) => {
    console.log(result);
    console.log("------Find one (PROJECTION) removing all the columns except gender...--------");
    console.log(result.emp_no);
    console.log("If we execute attributes.exclude = 'gender' correctly, result.gender should now be", result.gender);
  }
).catch(
  (error) => {
    console.log(error);
  }
);

//LIKE STATEMENT
Employees.findAll(
  {
    limit: 10,
    where: {
      first_name: {
        [Op.like]: 'Tzv%'
      }
    }
  }
).then(results => {
  console.log("------LIKE STATEMENT--------");
  results.forEach(
    (el)=> {
      console.log(el.dataValues);
    } 
  )
}).catch((error) => {
  console.log(error);
});

// OR CONDITION PART 2
Employees.findAll(
  {
    limit: 10,
    where: {
      [Op.or]: [
        {first_name: 'Saniya'},
        {first_name: 'Tzvetan'}
      ]
    }
  }
).then(
  (results) => {
    console.log("------OR STATEMENT--------");
    results.forEach(
      (el)=> {
        console.log(el.dataValues);
      } 
    )
  }
).catch(
  (error) => {
    console.log(error);
  }
);

// OR CONDITION PART 2
// Use the Op.eq seems redundant but it is necessary for security and prevent against SQL injections
Employees.findAll(
  {
    limit: 10,
    where: {
      [Op.or]: [
        {first_name: {
          [Op.eq]: 'Saniya'
        }},
        {first_name: {
          [Op.eq]: 'Tzvetan'
        }}
      ]
    }
  }
).then(
  (results) => {
    console.log("------OR STATEMENT--------");
    results.forEach(
      (el)=> {
        console.log(el.dataValues);
      }
    )
  }
).catch(
  (error) => {
    console.log(error);
  }
);

Employees.findAndCountAll({}).then(
  (result)=>{
    console.log("----------Find all and count no. of rows----------------");
    console.log(result.count);
  }
).catch(
  (error)=>{
    console.log(error);
  }
);

Employees.findOne(
  {where: {emp_no: 10008}}).then(
    (result)=>{
      result.update({last_name: 'Lee'}).then(
        () => {
          console.log("Record updated");
        }
      ).catch(
        (error) => {
          console.log(error)
        }
      )
    }
  ).catch(
    (error) => {
      console.log(error)
    }
  );

//Spread returns you a second argument whether. Spead is a Sequelize operator.
Employees.findOrCreate(
  {
    where: {
      emp_no: 1
    },
    defaults: {
      birth_date: new Date(),
      first_name: "Sarah",
      last_name: "Lee",
      gender: "F",
      hire_date: new Date()
    }
  }
).spread(
  (result, created) => {
    console.log(result);
  }
);
