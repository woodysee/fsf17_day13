const Sequelize = require("sequelize");
const SQL_USERNAME = "root";
const SQL_PASSWORD = "";

const connection = new Sequelize(
    'employees',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: false,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

connection.query(
    "SELECT * from employees LIMIT 3", {
        type: connection.QueryTypes.SELECT
    }
).then(
    (users) => {
        console.log("--------------SELECT * from employees LIMIT 3-------------");
        console.log("Check", users);
    }
);

connection.query(
    "SELECT * from employees where last_name = :last_name LIMIT 3",
        {
            replacements: {
                last_name: "Kalloufi"
            }
        },
        {
            type: connection.QueryTypes.SELECT
        }
).then(
    (users) => {
        console.log("--------------SELECT * from employees where last_name = :last_name LIMIT 3-------------");
        console.log("Check", users);
    }
);

connection.query(
    "SELECT * from employees where last_name like :last_name LIMIT 3",
        {
            replacements: {
                last_name: "%ng%"
            }
        },
        {
            type: connection.QueryTypes.SELECT
        }
).then(
    (users) => {
        console.log("------------SELECT * from employees where last_name like :last_name LIMIT 3---------------");
        console.log("Check:",users);
    }
);

connection.query(
    "UPDATE EMPLOYEES SET last_name = :last_name where emp_no = :emp_no",
    {
      replacements: {
        last_name: "Lee",
        emp_no: "10084"
      }
    },
    {
      type: connection.QueryTypes.SELECT
    }
  ).spread((res,metadata) => {
    console.log(
      "------------UPDATE EMPLOYEES SET last_name = :last_name where emp_no = :emp_no---------------"
    );
    console.log(res);
  });

connection.query(
    "INSERT INTO employees (emp_no, birth_date, first_name, last_name, gender, hire_date) VALUES (:emp_no, :birth_date, :first_name, :last_name, :gender, :hire_date)",
    {
        replacements: {
            emp_no: '300025',
            birth_date: new Date(),
            first_name: "Newguy",
            last_name: "Newgirl",
            gender: "M",
            hire_date: new Date()
        }
    },
    {
      type: connection.QueryTypes.INSERT
    }
).spread((res,metadata) => {
    console.log("-----------INSERT INTO employees: ---------------");
    console.log(res);
  });


connection.query(
    "DELETE FROM employees where emp_no = :emp_no",
    {
        replacements: {
            emp_no: '300025',
        }
    },
    {
      type: connection.QueryTypes.DELETE
    }
).spread((res,metadata) => {
    console.log("----------- DELETE FROM employees: ---------------");
    console.log(res);
});